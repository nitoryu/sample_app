class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      flash[:success] = "Welcome to Sample App! Take your time"
      log_in @user
      params[:session][:remember_me] == "1" ? remember(@user) : forget(@user)
      redirect_back_or @user
    else
      flash.now[:danger] = "Invalid email/password combination" # Not quite right!
      render :new
    end
  end

  def destroy
    log_out if logged_in?
    flash[:danger] = "You logged out. See you again!"
    redirect_to root_url
  end
end
